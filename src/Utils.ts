function convertPixelsToRem(px: number): number {
  return px / parseFloat(getComputedStyle(document.documentElement).fontSize.replace('px', ''))
}

function convertRemToPixels(rem: number): number {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize.replace('px', ''))
}

export { convertPixelsToRem, convertRemToPixels }
