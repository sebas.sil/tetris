import React from 'react'
import style from './Piece.module.css'

interface ITetrimino {
  readonly name: 'I' | 'O' | 'T' | 'S' | 'Z' | 'J' | 'L'
  color: 'cyan' | 'blue' | 'orange' | 'yellow' | 'green' | 'purple' | 'red'
  id: string
}

const Tetrimino: React.FC<ITetrimino & React.HTMLProps<HTMLDivElement>> = ({ color, id = '_0', name }) => {
  return (
    <div data-name={name} id={id} className={style.piece}>
      <div className={style.block} style={{ backgroundColor: color }}></div>
      <div className={style.block} style={{ backgroundColor: color }}></div>
      <div className={style.block} style={{ backgroundColor: color }}></div>
      <div className={style.block} style={{ backgroundColor: color }}></div>
    </div>
  )
}

export { Tetrimino }
export type { ITetrimino }
