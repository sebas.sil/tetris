import React, { useEffect } from 'react'
import { ITetrimino, Tetrimino } from '../ITetrimino'
import { convertRemToPixels } from '../../Utils'

const Box: React.FC<ITetrimino> = ({ id, ...others }) => {
  useEffect(() => {
    const master = document.querySelector(`#${id}`) as HTMLElement
    master.style.height = '2rem'
    const pieces = master?.querySelectorAll('div')

    if (pieces != null) {
      const incrementInPx = convertRemToPixels(1)
      const piece1 = pieces[1] as HTMLElement
      const piece2 = pieces[2] as HTMLElement
      const piece3 = pieces[3] as HTMLElement
      piece1.style.left = `${incrementInPx}px`
      piece2.style.top = `${incrementInPx}px`
      piece2.style.left = `${incrementInPx}px`
      piece3.style.top = `${incrementInPx}px`
    }
  }, [id])

  return (
    <Tetrimino
      style={{ height: `${convertRemToPixels(2)}px`, width: `${convertRemToPixels(2)}px` }}
      {...others}
      color='yellow'
      name='O'
      id={id}
    />
  )
}

export { Box }
