import React, { useEffect } from 'react'
import { convertRemToPixels } from '../../Utils'
import { ITetrimino, Tetrimino } from '../ITetrimino'

const Line: React.FC<ITetrimino> = ({ id, ...others }) => {
  useEffect(() => {
    const master = document.querySelector(`#${id}`)
    const pieces = master?.querySelectorAll('div')

    if (pieces != null) {
      const incrementInPx = convertRemToPixels(1)
      const piece1 = pieces[1] as HTMLElement
      const piece2 = pieces[2] as HTMLElement
      const piece3 = pieces[3] as HTMLElement
      piece1.style.top = `${incrementInPx}px`
      piece2.style.top = `${incrementInPx * 2}px`
      piece3.style.top = `${incrementInPx * 3}px`
    }
  }, [id])

  return (
    <Tetrimino
      style={{ height: `${convertRemToPixels(4)}px`, width: `${convertRemToPixels(4)}px` }}
      {...others}
      color='cyan'
      name='I'
      id={id}
    />
  )
}

export { Line }
