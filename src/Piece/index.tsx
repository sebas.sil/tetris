import React from 'react'
import { Line } from './I'
import { ITetrimino } from './ITetrimino'
import { Gamma } from './J'
import { Alpha } from './L'
import { Box } from './O'
import { RSnake } from './S'
import { Tee } from './T'
import { LSnake } from './Z'

const COLOR_MAP = {
  I: 'cyan',
  J: 'blue',
  L: 'orange',
  O: 'yellow',
  S: 'green',
  T: 'purple',
  Z: 'red'
} as const

type Keys = keyof typeof COLOR_MAP
type Values = typeof COLOR_MAP[Keys]

const TetrisPiece: React.FC<ITetrimino> = props => {
  function getPieceByName(): React.ReactElement {
    switch (props.name) {
      case 'I':
        return <Line {...props} />
      case 'J':
        return <Gamma {...props} />
      case 'L':
        return <Alpha {...props} />
      case 'O':
        return <Box {...props} />
      case 'S':
        return <RSnake {...props} />
      case 'T':
        return <Tee {...props} />
      default:
        return <LSnake {...props} />
    }
  }

  return getPieceByName()
}

class Piece {
  constructor(readonly name: Keys, readonly id: string) {}

  static getRandomPiece(): Piece {
    const keys = Object.keys(COLOR_MAP)
    const item = keys[Math.floor(Math.random() * keys.length)] as Keys
    return new Piece(item, `_${Math.floor(performance.now())}`)
  }

  getColor(): Values {
    return COLOR_MAP[this.name]
  }
}

export { TetrisPiece, Piece }
