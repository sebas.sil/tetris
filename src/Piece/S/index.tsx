import React, { useEffect } from 'react'
import { ITetrimino, Tetrimino } from '../ITetrimino'
import { convertRemToPixels } from '../../Utils'

const RSnake: React.FC<ITetrimino> = ({ id, ...others }) => {
  useEffect(() => {
    const master = document.querySelector(`#${id}`)
    const pieces = master?.querySelectorAll('div')

    if (pieces != null) {
      const incrementInPx = convertRemToPixels(1)
      const piece0 = pieces[0] as HTMLElement
      const piece1 = pieces[1] as HTMLElement
      const piece2 = pieces[2] as HTMLElement
      const piece3 = pieces[3] as HTMLElement

      piece0.style.top = `${incrementInPx}px`
      piece1.style.top = `${incrementInPx}px`
      piece1.style.left = `${incrementInPx}px`
      piece2.style.left = `${incrementInPx}px`
      piece3.style.left = `${incrementInPx * 2}px`
    }
  }, [id])

  return (
    <Tetrimino
      style={{ height: `${convertRemToPixels(3)}px`, width: `${convertRemToPixels(3)}px` }}
      {...others}
      color='green'
      name='S'
      id={id}
    />
  )
}

export { RSnake }
