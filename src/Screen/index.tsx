import React, { ReactElement, useEffect, useRef, useState } from 'react'
import style from './Screen.module.css'
import { convertRemToPixels } from '../Utils'
import { TetrisPiece, Piece } from '../Piece'

const Screen: React.FC = (): ReactElement => {
  const [currentPiece, setCurrentPiece] = useState<Piece>()
  const pieces = useRef<HTMLDivElement[]>([])

  function rotatePiece(evt: React.KeyboardEvent): void {
    const screen = document.querySelector('#screen')
    // const allPieces = screen?.querySelectorAll(':scope > div')
    // const screenSize = parseInt(getComputedStyle(document.documentElement).height)

    if (currentPiece != null) {
      const piece = screen?.querySelector(`#${currentPiece.id}`) as HTMLInputElement
      if (evt.key === ' ') {
        const oldValue = piece.style.transform === '' ? 0 : parseInt(piece.style.transform.replace(/[^\d]*/, ''))
        const newValue = (oldValue + 90) % 360
        piece.style.transform = `rotate(${newValue}deg)`
      } else if (evt.key === 'ArrowLeft') {
        const oldValue = piece.style.left === '' ? 0 : parseInt(piece.style.left)
        const newValue = oldValue - convertRemToPixels(1)
        piece.style.left = `${newValue}px`
      } else if (evt.key === 'ArrowRight') {
        const oldValue = piece.style.left === '' ? 0 : parseInt(piece.style.left)
        const newValue = oldValue + convertRemToPixels(1)
        piece.style.left = `${newValue}px`
      } else if (evt.key === 'ArrowDown') {
        const oldValue = piece.style.top === '' ? 0 : parseInt(piece.style.top)
        const newValue = oldValue + convertRemToPixels(1)
        piece.style.top = `${newValue}px`
      }
    }
  }

  const pieceMoviment = useRef<number>()

  useEffect(() => {
    // control game speed
    const screenSize = parseInt(getComputedStyle(document.documentElement).height)
    const screen = document.querySelector('#screen')

    pieces.current.forEach(p => screen?.appendChild(p))

    if (currentPiece != null) {
      pieceMoviment.current = setInterval(() => {
        const piece = document.querySelector(`#${currentPiece.id}`) as HTMLDivElement
        const oldValue = piece.style.top === '' ? 0 : parseInt(piece.style.top)
        const newValue = oldValue + convertRemToPixels(1)

        if (newValue < screenSize) {
          piece.style.top = `${newValue}px`
        } else {
          clearInterval(pieceMoviment.current)
          pieces.current = [...pieces.current, piece]
          setCurrentPiece(() => Piece.getRandomPiece())
        }
      }, 1000)
    } else {
      setCurrentPiece(() => new Piece('Z', '_1'))
    }
  }, [currentPiece, pieces])
  return (
    <div className={style.screen} id='screen' onKeyDown={rotatePiece} tabIndex={0}>
      {currentPiece !== undefined && (
        <TetrisPiece id={currentPiece.id} name={currentPiece.name} color={currentPiece.getColor()} />
      )}
    </div>
  )
}

export { Screen }
