import React from 'react'
import { Screen } from './Screen'

const App: React.FC = props => {
  return <Screen />
}

export default App
