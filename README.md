# Tetris #

This code is React version of Tetris game.

## techs ##

- React
- Eslint
- Prettier
- Vite
- Typescript


## Next steps:

1. [x] falling down pieces
2. [x] pieces
3. [ ] rotate
4. [ ] colision
    1. [ ] above
    2. [ ] when rotating
5. [ ] destroi complete line
6. [ ] score

## Images:
![screen image with pieces](./public/v0_screen.png)